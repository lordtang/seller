package com.imooc.sell.dto;


import lombok.Getter;

/**
 * 购物车
 */
@Getter
public class CartDto {
    private String productId;

    private Integer productQuantity;

    public CartDto(String productId, Integer productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
}
