package com.imooc.sell.exception;

import com.imooc.sell.enums.ResultEnum;

public class SellerException extends RuntimeException {
    private String code;

    public SellerException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode();
    }

    public SellerException(String code, String defaultMessage) {
        super(defaultMessage);
        this.code = code;
    }
}
