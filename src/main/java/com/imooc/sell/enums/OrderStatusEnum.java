package com.imooc.sell.enums;

import lombok.Getter;

import javax.management.loading.MLetContent;

@Getter
public enum OrderStatusEnum {
    FINISH(14,"已经完成"),
    WAIT_PAY(15,"等待支付"),
    NEW(16,"新订单"),
    CANCEL(17,"已取消");
    private Integer code;
    private String msg;

    OrderStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
