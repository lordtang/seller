package com.imooc.sell.enums;

import lombok.Getter;

@Getter
public enum ResultEnum {
    PRODUCT_NOT_EXIST("10","商品不存在"),
    PRODUCT_STOCK_ERROR("11","库存不足"),
    ORDER_DONT_EXIST("15","订单不存在"),
    ORDERDETAIL_NOT_EXIST("16","订单详情不存在"),
    ORDER_STATUS_ERROR("17","订单状态错误"),
    UPDATE_FAIL("18","订单更新失败"),
    ORDER_DETAIL_EMPTY("19","订单中无商品详情"),
    ORDER_PAY_STATUS_ERROR("20","订单支付状态错误"),
    PARAM_ERROR("21","参数错误"),
    CART_EMPTY("22","购物车为空"),
    ORDER_OWNER_ERROR("23","openid不一致");

    private String code;

    private String msg;

    ResultEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
