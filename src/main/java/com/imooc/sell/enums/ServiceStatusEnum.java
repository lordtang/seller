package com.imooc.sell.enums;

import lombok.Getter;

@Getter
public enum ServiceStatusEnum {
    OK("0000","成功"),
    FAIL("1111","失败")
    ;
    private String code;
    private String msg;

    ServiceStatusEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
