package com.imooc.sell.enums;

import lombok.Getter;

@Getter
public enum PayStatusEnum {
    NOT_PAY(12,"未支付"),
    PAID(13,"已经支付")
    ;
    private Integer code;
    private String msg;

    PayStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
