package com.imooc.sell.controller;

import com.imooc.sell.domain.Result;
import com.imooc.sell.dto.OrderDto;
import com.imooc.sell.enums.ResultEnum;
import com.imooc.sell.exception.SellerException;
import com.imooc.sell.service.IOrderService;
import com.imooc.sell.service.impl.OrderServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(value = "/seller")
public class SellerController {
    @Autowired
    private OrderServiceImpl orderService;

    @GetMapping(value = "/list")
    public Result getList(@RequestParam("page") Integer page,@RequestParam("size") Integer size){
        if(size<0){
            size=10;
        }
        Pageable pageable = PageRequest.of(page+1,size);
        Page<OrderDto> list = orderService.findList(pageable);
        return new Result("0000","获取成功",list);

    }


    @GetMapping("/cancel")
    public Result cancelOrder(@RequestParam("openId") String openId,@RequestParam("orderId") String orderId){
        OrderDto orderDto = orderService.queryOrder(orderId);
        if(!openId.equalsIgnoreCase(orderDto.getBuyerOpenId())){
            log.error("openid不一致，前端传来：{},后端查出:{}",openId,orderDto.getBuyerOpenId());
            throw new SellerException(ResultEnum.ORDER_OWNER_ERROR);
        }
        orderService.cancelOrder(orderDto);

        return new Result("0000","取消成功");
    }

}
