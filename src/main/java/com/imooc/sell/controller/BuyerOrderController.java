package com.imooc.sell.controller;

import com.imooc.sell.converter.OrderForm2OrderDtoConverter;
import com.imooc.sell.dataObject.OrderDetail;
import com.imooc.sell.domain.Result;
import com.imooc.sell.dto.OrderDto;
import com.imooc.sell.enums.OrderStatusEnum;
import com.imooc.sell.enums.ResultEnum;
import com.imooc.sell.enums.ServiceStatusEnum;
import com.imooc.sell.exception.SellerException;
import com.imooc.sell.form.OrderForm;
import com.imooc.sell.service.impl.OrderServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("buyer/order")
@Slf4j
public class BuyerOrderController {
    @Autowired
    private OrderServiceImpl orderService;
    //创建订单

    @RequestMapping(value = "/create",method = RequestMethod.POST)
    public Result create(@Valid OrderForm orderForm, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            log.error("[创建订单]参数不正确,orderForm={}",orderForm);
            throw new SellerException(ResultEnum.PARAM_ERROR.getCode(),bindingResult.getFieldError().getDefaultMessage());
        }

        OrderDto orderDto = OrderForm2OrderDtoConverter.convert(orderForm);
        if(CollectionUtils.isEmpty(orderDto.getOrderDetails())){
            log.error("购物车不能为空");
            throw new SellerException(ResultEnum.CART_EMPTY);
        }
        OrderDto result =  orderService.createOrder(orderDto);
        return new Result(ServiceStatusEnum.OK.getCode(),ServiceStatusEnum.OK.getMsg(),result);
    }


    //获取订单列表

   @GetMapping("/list")
    public Result getOrderList(@RequestParam("openid") String openid,
                               @RequestParam(value = "page",defaultValue = "0") Integer page,
                               @RequestParam(value = "size",defaultValue = "10") Integer size){

        if(StringUtils.isEmpty(openid)){
            log.error("[查询订单列表]openid为空");
            throw new SellerException(ResultEnum.PARAM_ERROR);
        }

       Pageable pageable =PageRequest.of(page,size);

       Page<OrderDetail> orderDetails = orderService.queryOrderList(openid, pageable);
       return new Result(ServiceStatusEnum.OK.getCode(),ServiceStatusEnum.OK.getMsg(),orderDetails.getContent());
   }
    //获取订单详情

    @GetMapping("/detail")
    public Result getDetail(@RequestParam("openid") String openid,
                            @RequestParam("orderId") String orderId){
        OrderDto orderDto = orderService.queryOrder(orderId);
        if(!orderDto.getBuyerOpenId().equalsIgnoreCase(openid)){
            log.error("openid和订单的openid不一致,订单openid:{},参数openid:{}",orderDto.getBuyerOpenId(),openid);
            throw new SellerException(ResultEnum.ORDER_OWNER_ERROR);
        }


        return new Result(ServiceStatusEnum.OK.getCode(),"获取成功",orderDto.getOrderDetails());
    }

    //取消订单

    @PostMapping("/cancel")
    public Result cancelOrder(@RequestParam("openid") String openid,
                              @RequestParam("orderId") String orderId){
        OrderDto orderDto = orderService.queryOrder(orderId);
        if(!orderDto.getBuyerOpenId().equalsIgnoreCase(openid)){
            log.error("openid和订单的openid不一致,订单openid:{},参数openid:{}",orderDto.getBuyerOpenId(),openid);
            throw new SellerException(ResultEnum.ORDER_OWNER_ERROR);
        }

        orderService.cancelOrder(orderDto);
        return new Result(ServiceStatusEnum.OK.getCode(),"成功");
    }
}
