package com.imooc.sell.controller;

import com.imooc.sell.dataObject.ProductInfo;
import com.imooc.sell.domain.Result;
import com.imooc.sell.domain.ResultCode;
import com.imooc.sell.service.ProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductInfoController {
    @Autowired
    private ProductInfoService productInfoService;

    @RequestMapping("/status/{status}")
    public Result findByProductStatus(@PathVariable Integer status){
        List<ProductInfo> byProductStatus = productInfoService.findByProductStatus(status);
        return new Result("0000", "获取成功",byProductStatus);
    }

    @RequestMapping("/buyer/order/create")
    public Result createOrder(){

        return null;
    }

}
