package com.imooc.sell.dao;


import com.imooc.sell.dataObject.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductCategoryDao extends JpaRepository<ProductCategory,Integer>, JpaSpecificationExecutor<ProductCategory> {
    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryIdList);
}
