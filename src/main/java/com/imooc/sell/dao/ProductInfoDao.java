package com.imooc.sell.dao;

import com.imooc.sell.dataObject.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductInfoDao extends JpaRepository<ProductInfo,String>, JpaSpecificationExecutor<ProductInfo> {
    List<ProductInfo> findByProductStatusIn(List<Integer> statusList);
    List<ProductInfo> findByProductStatus(Integer status);

}
