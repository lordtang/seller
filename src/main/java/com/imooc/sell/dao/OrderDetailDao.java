package com.imooc.sell.dao;

import com.imooc.sell.dataObject.OrderDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailDao extends JpaSpecificationExecutor<OrderDetail>, JpaRepository<OrderDetail,Integer> {
    List<OrderDetail> findByOrderId(String orderId);
}
