package com.imooc.sell.dao;

import com.imooc.sell.dataObject.Seller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SellerDao extends JpaRepository<Seller,Integer>, JpaSpecificationExecutor<Seller> {
}
