package com.imooc.sell.dao;

import com.imooc.sell.dataObject.OrderMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderMasterDao extends JpaSpecificationExecutor<OrderMaster>, JpaRepository<OrderMaster,Integer> {
    OrderMaster findByOrderId(String orderId);

    Page<OrderMaster> findOrderMasterByBuyerOpenId(String openid,Pageable pageable);
}
