package com.imooc.sell.util;

import java.util.Random;

public class KeyUtil {

    /*
    * 时间加上随机数
  * */
    public static synchronized String genUniqueKey(){
        Random random = new Random();
        long l = System.currentTimeMillis();
        Integer a =  random.nextInt(900000)+10000;

        return l+String.valueOf(a);
    }
}
