package com.imooc.sell.domain;

import lombok.Getter;

@Getter
public enum ResultCode {
    OK("0000"),
    FAIL("1111")
    ;
    private String code;

    ResultCode(String code) {
        this.code = code;
    }
}
