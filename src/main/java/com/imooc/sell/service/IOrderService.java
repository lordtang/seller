package com.imooc.sell.service;

import com.imooc.sell.dataObject.OrderDetail;
import com.imooc.sell.dto.OrderDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IOrderService {

    OrderDto createOrder(OrderDto orderDto);
    //
//    //取消订单
//
    OrderDto releaseOrder(Integer orderId);
    //    //查询订单列表
//
    Page<OrderDetail> queryOrderList(String buyerOpenId, Pageable pageable);
    //    //查询某个订单
//
    OrderDto queryOrder(String orderId);
    //
//    //取消订单
    OrderDto cancelOrder(OrderDto orderDto);
    //
//    //完结订单
    OrderDto finishOrder(OrderDto orderDto);
    //
//    //支付订单
//
    OrderDto paid (OrderDto orderDto);

    Page<OrderDto> findList(Pageable pageable);
}
