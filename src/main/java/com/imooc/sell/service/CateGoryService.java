package com.imooc.sell.service;


import com.imooc.sell.dataObject.ProductCategory;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CateGoryService {
    ProductCategory findOne(Integer categoryId);

    List<ProductCategory> findAll();

    List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryList);

    void updateCategory(ProductCategory productCategory);

    void save(ProductCategory productCategory);

    void delete(ProductCategory productCategory);
}
