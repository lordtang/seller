package com.imooc.sell.service.impl;


import com.imooc.sell.dao.OrderDetailDao;
import com.imooc.sell.dao.OrderMasterDao;
import com.imooc.sell.dao.ProductInfoDao;
import com.imooc.sell.dataObject.OrderDetail;
import com.imooc.sell.dataObject.OrderMaster;
import com.imooc.sell.dataObject.ProductInfo;
import com.imooc.sell.dto.CartDto;
import com.imooc.sell.dto.OrderDto;
import com.imooc.sell.enums.OrderStatusEnum;
import com.imooc.sell.enums.PayStatusEnum;
import com.imooc.sell.enums.ResultEnum;
import com.imooc.sell.exception.SellerException;
import com.imooc.sell.service.IOrderService;
import com.imooc.sell.service.ProductInfoService;
import com.imooc.sell.util.KeyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private OrderMasterDao orderMasterDao;

    @Autowired
    private OrderDetailDao orderDetailDao;

    @Autowired
    private ProductInfoDao productInfoDao;

    @Autowired
    private ProductInfoService productInfoService;

    @Override
    @Transactional
    public OrderDto createOrder(OrderDto orderDto) {
        String orderId = KeyUtil.genUniqueKey();

        BigDecimal orderAmount  = new BigDecimal(BigInteger.ZERO);
        //查询商品(数量，订单)
        List<OrderDetail> orderDetails = orderDto.getOrderDetails();
        for(OrderDetail orderDetail:orderDetails){
            ProductInfo productInfo = productInfoDao.findById(orderDetail.getProductId()).get();
            if(productInfo==null){
                throw new SellerException(ResultEnum.PRODUCT_NOT_EXIST);
            }

            //计算总价
            orderAmount = productInfo.getProductPrice().multiply(new BigDecimal(orderDetail.getProductQuantity())).add(orderAmount);

            //订单详情入库
            orderDetail.setDetailId(KeyUtil.genUniqueKey());
            orderDetail.setOrderId(orderId);
            BeanUtils.copyProperties(productInfo,orderDetail);
            System.out.println(orderDetail.toString());
            orderDetailDao.save(orderDetail);

        }

        //生成orderMaster
        orderDto.setOrderId(orderId);
        OrderMaster orderMaster = new OrderMaster();
        BeanUtils.copyProperties(orderDto,orderMaster);
        orderMaster.setOrderStatus(OrderStatusEnum.NEW.getCode());
        orderMaster.setPayStatus(PayStatusEnum.NOT_PAY.getCode());
        orderMaster.setOrderAmount(orderAmount);


        orderMasterDao.save(orderMaster);

        //扣库存
        List<CartDto> cartDtos = orderDto.getOrderDetails().stream().map(e -> new CartDto(e.getProductId(), e.getProductQuantity())).collect(Collectors.toList());
        productInfoService.decreaseStock(cartDtos);

        BeanUtils.copyProperties(orderMaster,orderDto);
        return orderDto;
    }

    @Override
    public OrderDto releaseOrder(Integer orderId) {
        return null;
    }

    @Override
    public Page<OrderDetail> queryOrderList(String buyerOpenId, Pageable pageable) {

        Specification<OrderDetail> specification = new Specification<OrderDetail>() {
            @Override
            public Predicate toPredicate(Root<OrderDetail> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
//                Path<Object> buyer_openid = root.get("buyer_openid");
//                Predicate equal = criteriaBuilder.equal(buyer_openid, buyerOpenId);
//                return equal;
                return null;
            }
        };

        return orderDetailDao.findAll(specification,pageable);
//        return null;
    }

    @Override
    public OrderDto queryOrder(String orderId) {
        OrderMaster orderMaster = orderMasterDao.findByOrderId(orderId);
        if(orderMaster==null){
            throw new SellerException(ResultEnum.ORDER_DONT_EXIST);
        }

        List<OrderDetail> list =  orderDetailDao.findByOrderId(orderId);
        if(CollectionUtils.isEmpty(list)){
            throw new SellerException(ResultEnum.ORDERDETAIL_NOT_EXIST);
        }

        OrderDto orderDto = new OrderDto();
        BeanUtils.copyProperties(orderMaster,orderDto);
        orderDto.setOrderDetails(list);
        orderDto.setOrderId(orderId);
        return orderDto;
    }

    @Override
    public OrderDto cancelOrder(OrderDto orderDto) {
        //判断订单状态
        OrderMaster orderMaster = new OrderMaster();
        BeanUtils.copyProperties(orderDto,orderMaster);
        Integer orderStatus = orderDto.getOrderStatus();

        //已经取消，已经完成的订单不能取消
        if(!orderStatus.equals(OrderStatusEnum.NEW.getCode())){
            log.error("订单号：{},状态码：{},状态：{}",orderDto.getOrderId(),orderDto.getOrderStatus(),ResultEnum.ORDER_STATUS_ERROR.getMsg());
            throw new SellerException(ResultEnum.ORDER_STATUS_ERROR);
        }
        //修改订单状态
        orderMaster.setOrderStatus(OrderStatusEnum.CANCEL.getCode());
        OrderMaster result = orderMasterDao.save(orderMaster);
        orderDto.setOrderStatus(OrderStatusEnum.CANCEL.getCode());
        if(result==null){
            log.error("[取消订单]更新失败{}",result.toString());
            throw new SellerException(ResultEnum.UPDATE_FAIL);
        }
        //返还库存
        if(CollectionUtils.isEmpty(orderDto.getOrderDetails())){
            log.error("[取消订单]订单中无商品详情");
            throw new SellerException(ResultEnum.ORDER_DETAIL_EMPTY);
        }
        List<CartDto> cartDtos = orderDto.getOrderDetails().stream().map(
                e->new CartDto(e.getProductId(),e.getProductQuantity())).collect(Collectors.toList());

        productInfoService.increaseStock(cartDtos);
        //如果已经支付，需要退款
        if(orderDto.getPayStatus().equals(PayStatusEnum.PAID)){
            //TODO
        }
        return orderDto;
    }

    @Override
    public OrderDto finishOrder(OrderDto orderDto) {
        //判断订单状态
        if(!orderDto.getOrderStatus().equals(OrderStatusEnum.NEW.getCode())){
            log.error("[完结订单]，订单号：{},订单状态：{}",orderDto.getOrderId(),orderDto.getOrderStatus());
            throw new SellerException(ResultEnum.ORDER_STATUS_ERROR);
        }
        OrderMaster orderMaster = new OrderMaster();
        orderDto.setOrderStatus(OrderStatusEnum.FINISH.getCode());
        BeanUtils.copyProperties(orderDto,orderMaster);

        //修改状态完结订单
        orderMasterDao.save(orderMaster);


        return orderDto;
    }

    @Override
    public OrderDto paid(OrderDto orderDto) {
        //判断订单状态
        if(!orderDto.getOrderStatus().equals(OrderStatusEnum.NEW.getCode())){
            log.error("[支付订单]，订单号：{},订单状态：{}",orderDto.getOrderId(),orderDto.getOrderStatus());
            throw new SellerException(ResultEnum.ORDER_STATUS_ERROR);
        }
        //判断支付状态
        if(!orderDto.getPayStatus().equals(PayStatusEnum.NOT_PAY.getCode())){
            log.error("[订单状态不是未支付]订单号：{}，订单状态：{}",orderDto.getOrderId(),orderDto.getPayStatus());
            throw new SellerException(ResultEnum.ORDER_PAY_STATUS_ERROR);
        }

        //修改状态完结订单,新订单,已经支付
        OrderMaster orderMaster = new OrderMaster();
        orderDto.setPayStatus(PayStatusEnum.PAID.getCode());
        BeanUtils.copyProperties(orderDto,orderMaster);
        orderMasterDao.save(orderMaster);

        return orderDto;
    }

    @Override
    public Page<OrderDto> findList(Pageable pageable) {
        Page<OrderMaster> orderMaster = orderMasterDao.findAll(pageable);
        List<OrderMaster> content = orderMaster.getContent();
        OrderDto orderDto = new OrderDto();
        List<OrderDto> list = new ArrayList<>();
        for(int i=0;i<content.size();i++){
            OrderMaster temp = content.get(i);
            BeanUtils.copyProperties(temp,orderDto);
            list.add(orderDto);
        }
        return new PageImpl<OrderDto>(list,pageable,orderMaster.getTotalElements());
    }
}
