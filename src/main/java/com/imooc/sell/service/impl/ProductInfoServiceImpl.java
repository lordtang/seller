package com.imooc.sell.service.impl;

import com.imooc.sell.dao.ProductInfoDao;
import com.imooc.sell.dataObject.ProductInfo;
import com.imooc.sell.dto.CartDto;
import com.imooc.sell.enums.ProductStatusEnums;
import com.imooc.sell.enums.ResultEnum;
import com.imooc.sell.exception.SellerException;
import com.imooc.sell.service.ProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductInfoServiceImpl implements ProductInfoService {
    @Autowired
    private ProductInfoDao productInfoDao;
    @Override
    public List<ProductInfo> findByProductStatusIn(List<Integer> statusList) {
        return productInfoDao.findByProductStatusIn(statusList);
    }

    @Override
    public List<ProductInfo> findByProductStatus(Integer status) {
        return productInfoDao.findByProductStatus(status);
    }

    @Override
    public List<ProductInfo> findUpAll() {
        return productInfoDao.findByProductStatus(ProductStatusEnums.UP.getCode());
    }

    @Override
    public List<ProductInfo> findAll(Pageable pageable) {
        return productInfoDao.findAll(pageable).getContent();
    }

    @Override
    public ProductInfo save(ProductInfo productInfo) {
        return productInfoDao.save(productInfo);
    }

    @Override
    public void increaseStock(List<CartDto> cartDtos) {
        for(CartDto cartDto :cartDtos){
            ProductInfo productInfo = productInfoDao.findById(cartDto.getProductId()).get();
            if(productInfo == null){
                throw new SellerException(ResultEnum.PRODUCT_NOT_EXIST);
            }
            Integer result = productInfo.getProductStock() + cartDto.getProductQuantity();
            productInfo.setProductStock(result);
            productInfoDao.save(productInfo);
        }
    }



    @Override
    @Transactional
    public void decreaseStock(List<CartDto> cartDtos) {
        for(CartDto cartDto :cartDtos){
            ProductInfo productInfo = productInfoDao.findById(cartDto.getProductId()).get();
            if(productInfo == null){
                throw new SellerException(ResultEnum.PRODUCT_NOT_EXIST);
            }
            Integer result = productInfo.getProductStock() - cartDto.getProductQuantity();
            if(result<0){
                throw new SellerException(ResultEnum.PRODUCT_STOCK_ERROR);
            }
            productInfo.setProductStock(result);
            productInfoDao.save(productInfo);
        }
    }
}
