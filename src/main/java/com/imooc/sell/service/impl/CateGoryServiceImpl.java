package com.imooc.sell.service.impl;

import com.imooc.sell.dao.ProductCategoryDao;
import com.imooc.sell.dataObject.ProductCategory;
import com.imooc.sell.service.CateGoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CateGoryServiceImpl implements CateGoryService {

    @Autowired
    private ProductCategoryDao productCategoryDao;

    @Override
    public ProductCategory findOne(Integer categoryId) {
        return productCategoryDao.findById(categoryId).get();
    }

    @Override
    public List<ProductCategory> findAll() {
        return productCategoryDao.findAll();
    }

    @Override
    public List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryList) {
        return productCategoryDao.findByCategoryTypeIn(categoryList);
    }

    @Override
    public void updateCategory(ProductCategory productCategory) {
        productCategoryDao.save(productCategory);

    }

    @Override
    public void save(ProductCategory productCategory) {
        productCategoryDao.save(productCategory);

    }

    @Override
    public void delete(ProductCategory productCategory) {
        productCategoryDao.delete(productCategory);

    }
}
