package com.imooc.sell.service;


import com.imooc.sell.dataObject.ProductInfo;
import com.imooc.sell.dto.CartDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductInfoService {
    List<ProductInfo> findByProductStatusIn(List<Integer> statusList);

    List<ProductInfo> findByProductStatus(Integer status);

    List<ProductInfo> findUpAll();

    List<ProductInfo> findAll(Pageable pageable);

    ProductInfo save(ProductInfo productInfo);

    //加库存

    void increaseStock(List<CartDto> cartDtos);


    //减库存
    void decreaseStock(List<CartDto> cartDtos);
}
