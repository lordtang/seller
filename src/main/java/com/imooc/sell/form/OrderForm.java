package com.imooc.sell.form;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class OrderForm {

    @NotEmpty(message = "姓名必须填写")
    private String name;

    @NotEmpty(message = "电话号码不能为空")
    private String phone;

    @NotEmpty(message = "地址为必填项")
    private String address;

    @NotEmpty(message = "openid必须填写")
    private String openid;

    @NotEmpty(message = "购物车items为空")
    private String items;
}
