package com.imooc.sell.dao;

import com.imooc.sell.dataObject.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCategoryDaoTest {
    @Autowired
    private ProductCategoryDao repository;


    @Transactional
    @Rollback(value = false)
    @Test
    public void testFindOne(){

        ProductCategory productCategory = new ProductCategory();
        productCategory.setCategoryName("aoligei");
        productCategory.setCategoryType(3);
        repository.save(productCategory);
        List<ProductCategory> all = repository.findAll();
        System.out.println(all.size());

    }


    @Test
    public void testFindBy(){
        List<Integer> list = Arrays.asList(2,3,4);
        List<ProductCategory> lists = repository.findByCategoryTypeIn(list);
        System.out.println(lists.size());
    }

}