package com.imooc.sell.service;

import com.imooc.sell.dao.ProductInfoDao;
import com.imooc.sell.dataObject.ProductInfo;
import com.imooc.sell.util.KeyUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Random;
import java.util.UUID;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductInfoServiceTest {
    @Autowired
    private ProductInfoDao productInfoDao;

    @Test
    @Transactional
    @Rollback(value = false)
    public void testSave(){
        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductId(KeyUtil.genUniqueKey());
//        productInfo.setProductId(UUID.randomUUID().toString());
        productInfo.setCategoryType(new Random().nextInt(2));
        productInfo.setProductStatus(new Random().nextInt(2));
        productInfo.setProductName("iphone");
        productInfo.setProductDescription("一款手机");
        productInfo.setProductIcon("phone");
        productInfo.setProductStock(10);
        productInfo.setProductPrice(new BigDecimal(new Random().nextInt(6000)));
        productInfoDao.save(productInfo);
    }

    @Test
    public void testMany(){
        for(int i=0;i<20;i++){
            this.testSave();
        }

    }

}