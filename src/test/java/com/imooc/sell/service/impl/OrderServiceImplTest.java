package com.imooc.sell.service.impl;

import com.imooc.sell.dataObject.OrderDetail;
import com.imooc.sell.dto.OrderDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceImplTest {
    @Autowired
    private OrderServiceImpl orderService;

    private final String buyerOpenID = "110110";
    @Test
    @Transactional
    @Rollback(value = false)
    public void createOrder() {
        Random random = new Random();
        String buyerName= RandomStringUtils.randomAlphanumeric(5);
        String address= RandomStringUtils.randomAlphanumeric(5);
        Long num =10000000000L+random.nextInt(1000000);
        OrderDto orderDto = new OrderDto();
        orderDto.setOrderId(String.valueOf(num));
        orderDto.setBuyerAddress(address);
        orderDto.setBuyerName(buyerName);
        orderDto.setBuyerPhone(num.toString());
        orderDto.setBuyerOpenId(buyerOpenID);
        List<OrderDetail> orderDetails = new ArrayList<>();
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setProductId("157424385286297510");
        orderDetail.setProductQuantity(5);
        orderDetails.add(orderDetail);
        orderDto.setOrderDetails(orderDetails);
        OrderDto result = orderService.createOrder(orderDto);
        log.error("创建订单：{}",result.toString());
    }

    @Test
    public void queryOrder(){
        OrderDto orderDto = orderService.queryOrder("157392943519963800");
        System.out.println(orderDto);
    }

    @Test
    public void testCre(){
        for(int i = 0;i<10;i++){
            this.createOrder();
        }

    }

    @Test
    public void testCancel(){
        OrderDto orderDto = orderService.queryOrder("1573929600491373349");
        OrderDto result = orderService.cancelOrder(orderDto);
        System.out.println(result.getOrderStatus());
    }


    @Test
    public void testFinish(){
        OrderDto orderDto = orderService.queryOrder("1573929600491373349");
        OrderDto result = orderService.finishOrder(orderDto);
        System.out.println(result);
    }

    @Test
    public void testPaid(){
        OrderDto orderDto = orderService.queryOrder("1573929627026901710");
        OrderDto result = orderService.paid(orderDto);
        System.out.println(result);
    }

    @Test
    public void testOs(){
        Pageable pageable = PageRequest.of(0,5);

        Page<OrderDto> list = orderService.findList(pageable);
        System.out.println(list.getContent().size());
    }

}